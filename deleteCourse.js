let params = new URLSearchParams(window.location.search);

let courseId = params.get("courseId");

fetch(`http://localhost:4000/courses/${courseId}`, {
  method: "DELETE",
  headers: {
    "Content-Type": "application/json",
    Authorization: `Bearer ${token}`,
  },
})
  .then((res) => res.json())
  .then((data) => {
    if (data) {
      alert("Course has been disabled successfully");
      window.location.replace("./courses.html");
    } else {
      alert("Error: Something went wrong please try again");
    }
  });
